package com.geoni.lab3;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    TextView Text;
    int n=0;
    Button button1;
    Button button2;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    Text = findViewById(R.id.textView);
    //button 2
    button1 = findViewById(R.id.button4);
    button2 = findViewById(R.id.button5);

        button2.setOnClickListener(view -> {
            n++;
            Text.setText(String.valueOf(n));
        });

        button1.setOnClickListener(view -> {
            n++;
            Text.setText(String.valueOf(n));
        });
    }
}